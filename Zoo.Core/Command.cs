﻿using System;
using System.Collections.Generic;
using System.Text;
using Zoo.Core.Contracts;

namespace Zoo.Core
{
    public abstract class Command : ICommand
    {
        protected Command(List<string> parameters, IDatabase database, IFactory factory)
        {
            this.Parameters = new List<string>(parameters);
            this.Factory = factory;
            this.Database = database;
        }

        public abstract string Execute();

        protected List<string> Parameters { get; }

        protected IDatabase Database { get; }

        protected IFactory Factory { get; }
    }
}
