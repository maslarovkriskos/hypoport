﻿using System.Collections.Generic;
using System.Linq;
using Zoo.Core.Contracts;

namespace Zoo.Core.Commands
{
    public class HowManyAlive : Command
    {
        public HowManyAlive(List<string> parameters, IDatabase database, IFactory factory) : base(parameters, database, factory)
        {
        }

        public override string Execute()
        {
            var bears = Database.Bears.Where(x => x.IsDeath == false).ToList();
            var giraffes = Database.Giraffes.Where(x => x.IsDeath == false).ToList();
            var monkeys = Database.Monkeys.Where(x => x.IsDeath == false).ToList();

            return $"The number of all living animals is: {bears.Count + giraffes.Count + monkeys.Count}";

        }
    }
}
