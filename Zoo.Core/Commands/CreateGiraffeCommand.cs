﻿using System;
using System.Collections.Generic;
using System.Text;
using Zoo.Core.Contracts;
using Zoo.Models.Models;

namespace Zoo.Core.Commands
{
    public class CreateGiraffeCommand : Command
    {
        private readonly IFactory _factory;

        public CreateGiraffeCommand(List<string> parameters, IDatabase database, IFactory factory) : base(parameters, database, factory)
        {
            this._factory = factory;
        }

        public override string Execute()
        {
            Giraffe giraffe = this._factory.CreateGiraffe("Giraffe", 100, false);
            this.Database.Giraffes.Add(giraffe);
            this.Database.Animals.Add(giraffe);
            return giraffe.ToString();
        }
    }
}
