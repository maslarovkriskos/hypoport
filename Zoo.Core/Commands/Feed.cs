﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zoo.Core.Contracts;

namespace Zoo.Core.Commands
{
    public class Feed : Command
    {
        public Feed(List<string> parameters, IDatabase database, IFactory factory) : base(parameters, database, factory)
        {
        }

        public override string Execute()
        {
            var animals = Database.Animals.Where(x => x.IsDeath == false).ToList();
            animals = animals.OrderByDescending(x => x.Health).ToList();

            int toFeed = (int)Math.Floor(animals.Count * 0.9);

            var animalsToFeed = animals.Take(toFeed).ToList();

            var random = new Random();
            var toIncreaseBearsHealth = random.Next(10, 25);

            foreach (var animal in animalsToFeed)
            {
                animal.Health += toIncreaseBearsHealth;
                if (animal.Health > 100)
                {
                    animal.Health = 100;
                }
            }

            return "Animals have been fed!";
        }
    }
}
