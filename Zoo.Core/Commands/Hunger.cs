﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zoo.Core.Contracts;

namespace Zoo.Core.Commands
{
    public class Hunger : Command
    {
        public Hunger(List<string> parameters, IDatabase database, IFactory factory) : base(parameters, database, factory)
        {
        }

        public override string Execute()
        {
            var random = new Random();
            var toDecreaseBearsHealth = random.Next(15, 35);

            var bears = this.Database.Bears.Where(x => x.IsDeath == false).ToList();
            foreach (var bear in bears)
            {
                bear.Health -= toDecreaseBearsHealth;
                if (bear.Health < 65)
                {
                    bear.IsDeath = true;
                }
            }

            var toDecreaseGiraffesHealth = random.Next(15, 35);
            var giraffes = this.Database.Giraffes.Where(x => x.IsDeath == false).ToList();
            foreach (var giraffe in giraffes)
            {
                giraffe.Health -= toDecreaseGiraffesHealth;
                if (giraffe.Health < 60)
                {
                    giraffe.IsDeath = true;
                }
            }

            var toDecreaseMonkeysHealth = random.Next(15, 35);
            var monkeys = this.Database.Monkeys.Where(x => x.IsDeath == false).ToList();
            foreach (var monkey in monkeys)
            {
                monkey.Health -= toDecreaseMonkeysHealth;
                if (monkey.Health < 40)
                {
                    monkey.IsDeath = true;
                }
            }

            return "Most of the animals are starving. Feed them!";
        }
    }
}
