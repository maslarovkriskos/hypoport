﻿using System;
using System.Collections.Generic;
using System.Text;
using Zoo.Core.Contracts;
using Zoo.Models.Models;

namespace Zoo.Core.Commands
{
    public class CreateMonkeyCommand : Command
    {
        private readonly IFactory _factory;

        public CreateMonkeyCommand(List<string> parameters, IDatabase database, IFactory factory) : base(parameters, database, factory)
        {
            this._factory = factory;
        }

        public override string Execute()
        {
            Monkey monkey = this._factory.CreateMonkey("Monkey", 100, false);
            this.Database.Monkeys.Add(monkey);
            this.Database.Animals.Add(monkey);
            return monkey.ToString();
        }
    }
}
