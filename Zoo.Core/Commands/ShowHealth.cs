﻿using System.Collections.Generic;
using System.Linq;
using Zoo.Core.Contracts;

namespace Zoo.Core.Commands
{
    public class ShowHealth : Command
    {
        public ShowHealth(List<string> parameters, IDatabase database, IFactory factory) : base(parameters, database, factory)
        {
        }

        public override string Execute()
        {
            int health = 0;
            //I am not sure if you want the minimal health or hot but that's the way I undestand it.
            //I take the count of bears and multyply it by 65(which is the minimal health of the bear before dyeing according to the task).
            if (this.Parameters[0] == "Bears")
            {
                var bears = this.Database.Bears.Where(x => x.IsDeath == false).ToList();

                health = bears.Count * 65;
            }
            else if (this.Parameters[0] == "Giraffes")
            {
                var giraffes = this.Database.Giraffes.Where(x => x.IsDeath == false).ToList();

                health = giraffes.Count * 60;
            }
            else if (this.Parameters[0] == "Monkeys")
            {
                var monkeys = this.Database.Monkeys.Where(x => x.IsDeath == false).ToList();

                health = monkeys.Count * 40;
            }

            return $"The sum of all living {Parameters[0]} health is : {health}.";
        }
    }
}
