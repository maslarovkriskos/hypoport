﻿using System;
using System.Collections.Generic;
using System.Text;
using Zoo.Core.Contracts;
using Zoo.Models.Models;

namespace Zoo.Core.Commands
{
    public class CreateBearCommand : Command
    {
        private readonly IFactory _factory;

        public CreateBearCommand(List<string> parameters, IDatabase database, IFactory factory) : base(parameters, database, factory)
        {
            this._factory = factory;
        }

        public override string Execute()
        {
            Bear bear = this._factory.CreateBear("Bear", 100, false);
            this.Database.Bears.Add(bear);
            this.Database.Animals.Add(bear);
            return bear.ToString();
        }
    }
}
