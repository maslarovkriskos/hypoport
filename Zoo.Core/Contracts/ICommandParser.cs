﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoo.Core.Contracts
{
    public interface ICommandParser
    {
        ICommand ParseCommand(string command, IDatabase database, IFactory factory);

    }
}
