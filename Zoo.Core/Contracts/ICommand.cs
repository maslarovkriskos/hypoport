﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoo.Core.Contracts
{
    public interface ICommand
    {
        string Execute();

    }
}
