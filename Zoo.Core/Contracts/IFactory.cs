﻿using System;
using System.Collections.Generic;
using System.Text;
using Zoo.Models.Contracts;
using Zoo.Models.Models;

namespace Zoo.Core.Contracts
{
    public interface IFactory
    {
        Bear CreateBear(string species, int health, bool isDeath);
        Giraffe CreateGiraffe(string species, int health, bool isDeath);
        Monkey CreateMonkey(string species, int health, bool isDeath);
    }
}
