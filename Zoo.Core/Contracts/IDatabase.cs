﻿using System;
using System.Collections.Generic;
using System.Text;
using Zoo.Models.Models;

namespace Zoo.Core.Contracts
{
    public interface IDatabase
    {
        List<Bear> Bears { get; }
        
        List<Giraffe> Giraffes { get; }
        
        List<Monkey> Monkeys { get; }
        
        List<Animal> Animals { get; }
    }
}
