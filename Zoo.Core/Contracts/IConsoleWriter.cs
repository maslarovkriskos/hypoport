﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoo.Core.Contracts
{
    public interface IConsoleWriter
    {
        string WriteLine(string output);

        string WriteLine();

        string Write(string output);

        void Clear();
    }
}
