﻿using System;
using Zoo.Core.Contracts;
using Zoo.Models.Models;

namespace Zoo.Core
{
    public class Factory : IFactory
    {
        public Bear CreateBear(string species, int health, bool isDeath)
        {
            return new Bear(species, health, isDeath);
        }

        public Giraffe CreateGiraffe(string species, int health, bool isDeath)
        {
            return new Giraffe(species, health, isDeath);
        }


        public Monkey CreateMonkey(string species, int health, bool isDeath)
        {
            return new Monkey(species, health, isDeath);
        }
    }

}
