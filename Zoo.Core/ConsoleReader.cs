﻿using System;
using Zoo.Core.Contracts;

namespace Zoo.Core
{
    public class ConsoleReader : IConsoleReader
    {
        public string Read()
        {
            return Console.ReadLine();
        }
    }
}
