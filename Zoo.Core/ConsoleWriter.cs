﻿using System;
using System.Collections.Generic;
using System.Text;
using Zoo.Core.Contracts;

namespace Zoo.Core
{
    public class ConsoleWriter : IConsoleWriter
    {
        public void Clear()
        {
            Console.Clear();
        }

        public string Write(string output)
        {
            Console.Write(output);
            return output;
        }

        public string WriteLine(string output)
        {
            Console.WriteLine(output);
            return $"{output}{Environment.NewLine}";
        }

        public string WriteLine()
        {
            Console.WriteLine();
            return Environment.NewLine;
        }
    }
}
