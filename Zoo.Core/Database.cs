﻿using System;
using System.Collections.Generic;
using System.Text;
using Zoo.Core.Contracts;
using Zoo.Models.Contracts;
using Zoo.Models.Models;

namespace Zoo.Core
{
    public class Database : IDatabase
    {
        public Database()
        {
            this.Bears = new List<Bear>();
            this.Giraffes = new List<Giraffe>();
            this.Monkeys = new List<Monkey>();
            this.Animals = new List<Animal>();

        }

        public List<Bear> Bears { get; }

        public List<Giraffe> Giraffes { get; }

        public List<Monkey> Monkeys { get; }

        public List<Animal> Animals { get; }
    }
}
