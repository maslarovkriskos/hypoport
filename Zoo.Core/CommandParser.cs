﻿using System;
using System.Collections.Generic;
using System.Text;
using Zoo.Core.Commands;
using Zoo.Core.Contracts;

namespace Zoo.Core
{
    public class CommandParser : ICommandParser
    {
        private readonly IConsoleReader reader;
        private readonly IConsoleWriter writer;

        public CommandParser(IConsoleReader reader, IConsoleWriter writer)
        {
            this.writer = writer;
            this.reader = reader;
        }

        public ICommand ParseCommand(string command, IDatabase database, IFactory factory)
        {
            List<string> parameters = new List<string>();

            switch (command)
            {
                // Create Commands
                case "CreateBear":

                    writer.WriteLine("Bear has been created!");
                    return new CreateBearCommand(parameters, database, factory);

                case "CreateGiraffe":

                    writer.WriteLine("Giraffe has been created!");
                    return new CreateGiraffeCommand(parameters, database, factory);

                case "CreateMonkey":

                    writer.WriteLine("Monkey has been created!");
                    return new CreateMonkeyCommand(parameters, database, factory);

                // Remove Commands
                case "Hunger":
                    writer.WriteLine("The animals are hungry.");
                    return new Hunger(parameters, database, factory);

                case "Feed":

                    var feed = new Feed(parameters, database, factory);
                    writer.WriteLine(feed.Execute());
                    return feed;

                case "ShowHealth":

                    writer.WriteLine("Which species health you would like to see?");
                    writer.WriteLine();
                    writer.WriteLine("For bears enter: Bears");
                    writer.WriteLine("For giraffes enter: Giraffes");
                    writer.WriteLine("For monkeys enter: Monkeys");
                    parameters.Add(reader.Read());

                    var show = new ShowHealth(parameters, database, factory);

                    writer.WriteLine(show.Execute());

                    return show;

                case "HowManyAlive":

                    var alive = new HowManyAlive(parameters, database, factory);
                    writer.WriteLine(alive.Execute());
                    return alive;


                default:

                    throw new InvalidOperationException("Command does not exist");
            }
        }
    }
}
