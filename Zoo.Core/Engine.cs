﻿using System;
using System.Collections.Generic;
using System.Text;
using Zoo.Core.Contracts;

namespace Zoo.Core
{
    public class Engine : IEngine
    {
        private readonly IDatabase database;
        private readonly IFactory factory;
        private readonly ICommandParser commandParser;
        private readonly IConsoleReader reader;
        private readonly IConsoleWriter writer;

        private string lastStatus = "Waiting for input. Enter to submit.";

        public Engine(IFactory factory, IDatabase database, IConsoleReader reader, IConsoleWriter writer)
        {
            this.writer = writer;
            this.reader = reader;
            this.factory = factory;
            this.database = database;
            this.commandParser = new CommandParser(reader, writer);
        }

        public void Run()
        {
            Console.WriteLine(InitialMessage());
            RunMainMenu();
        }

        private void RunMainMenu()
        {
            string input = reader.Read();

            if (input.ToLower() == "exit")
            {
                Environment.Exit(0);
            }
            
            this.Parse(input);

            RunMainMenu();
        }

       
        private void Parse(string input)
        {
            try
            {
                ICommand command = this.commandParser.ParseCommand(input, database, factory);

                this.lastStatus = command.Execute();

              // reader.Read();
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                this.lastStatus = $"ERROR: {e.Message}";
            }

        }

        private string InitialMessage()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Please type some of the following commands:");
            sb.AppendLine("1.  CreateBear");
            sb.AppendLine("2.  CreateMonkey");
            sb.AppendLine("3.  CreateGiraffe");
            sb.AppendLine("4.  Feed");
            sb.AppendLine("5.  Hunger");
            sb.AppendLine("6.  ShowHealth");
            sb.AppendLine("7.  HowManyAlive");
            sb.AppendLine("########################################");
            sb.AppendLine();
            return sb.ToString();
        }
    }
}
