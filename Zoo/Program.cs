﻿using System;
using Zoo.Core;
using Zoo.Core.Contracts;

namespace Zoo
{
    class Program
    {
        private static readonly IFactory factory = new Factory();
        private static readonly IDatabase database = new Database();
        private static readonly IConsoleReader reader = new ConsoleReader();
        private static readonly IConsoleWriter writer = new ConsoleWriter();
        private static readonly IEngine engine = new Engine(factory, database, reader, writer);

        static void Main()
        {
            engine.Run();
        }
    }
}
