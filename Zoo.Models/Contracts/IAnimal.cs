﻿namespace Zoo.Models.Contracts
{
    public interface IAnimal
    {
        public string Species { get; }
        public int Health { get; }
        public bool IsDeath { get; }
        
    }
}
