﻿using Zoo.Models.Contracts;

namespace Zoo.Models.Models
{
    public class Animal : IAnimal
    {
        public string Species { get; }

        public int Health { get; set; }

        public bool IsDeath { get; set; }
    }
}
