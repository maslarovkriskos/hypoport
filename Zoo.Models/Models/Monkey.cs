﻿using Zoo.Models.Contracts;

namespace Zoo.Models.Models
{
    public class Monkey : Animal
    {
        public Monkey(string species, int health, bool isDeath)
        {
            this.Species = species;
            this.Health = health;
            this.IsDeath = isDeath;
        }
        public string Species { get; }

        public int Health { get; set; }

        public bool IsDeath { get; set; }
    }
}
