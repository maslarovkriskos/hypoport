﻿using Zoo.Models.Contracts;

namespace Zoo.Models.Models
{
    public class Bear : Animal
    {
        public Bear(string species, int health, bool isDeath)
        {
            this.Species = species;
            this.Health = health;
            this.IsDeath = isDeath;
        }
        public new string Species { get; }

        public new int Health { get; set; }

        public new bool IsDeath { get; set; }
    }
}
