1. The advantages of inheritance are:
-Code reusability
-Inheritance provide a clear model structure which is easy to understand
-Using inheritance you can build a clean hierarchical structure.

The disadvantages of inheritance are:
-You can't modify an inherited implementation at runtime
-There is a chance of unexperienced developer to increase the complexity of the program.

2. Generics allow you to create a class, method field or whatever you need with no specified data type. 
You can pick up the needed data when creating instance.

3. I would prefer Dictionary. I think it uses hash table and the sorted dictionary uses tree, which means it should be faster for almost everything.

4. I would say that the momery management is done by the garbage collector. It works like this:
- Firstly itterates over all instances and if some of them have no references - marks them.
- Then in the seccond itteration if they still have no references - they are cleared, otherwise they got unmarked.